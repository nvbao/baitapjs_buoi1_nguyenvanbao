/**
 * Tính chu vi, diện tích HCN
 * input: nhập chiều dài, rộng
 *tạo biến lưu: chiều dài, rộng, diện tích, chu vi
 *output: diện tích: 50, chu vi: 30
 */
var dai = 10;
var rong = 5;
var dienTich = dai * rong;
var chuVi = (dai + rong) * 2;
console.log({ dienTich }, { chuVi });
